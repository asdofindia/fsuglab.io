var mymap = L.map('mapid').setView([21, 78], 4);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

var mapMarker = L.icon({
  iconUrl: 'img/marker-icon.png',
  shadowUrl: 'img/marker-shadow.png',
  iconSize: [20, 30],
  shadowSize: [30, 30],
  shadowAnchor: [10, 14]
});

var goa = L.marker([15.49, 73.82], {
  icon: mapMarker
}).addTo(mymap).bindPopup('<b><a href="http://goa.fsug.in" target="__blank" style="text-decoration: none">FSUG Goa</a></b>');

var coep = L.marker([18.5293994,73.8560156], {
  icon: mapMarker
}).addTo(mymap).bindPopup('<b><a href="http://co.fsug.in" target="__blank" style="text-decoration: none">FSUG at College of Engineering, Pune</a></b>');

var plug = L.marker([18.519368, 73.855321], {
  icon: mapMarker
}).addTo(mymap).bindPopup('<b><a href="http://plug.org.in" target="__blank" style="text-decoration: none">Pune GNU/Linux Users Group, Pune</a></b>');

var thrissur = L.marker([10.52, 76.21], {
  icon: mapMarker
}).addTo(mymap).bindPopup('<b><a href="http://thrissur.fsug.in" target="__blank" style="text-decoration: none">FSUG Thrissur</a></b>');

var calicut = L.marker([11.25, 75.78], {
  icon: mapMarker
  }).addTo(mymap).bindPopup('<b><a href="http://calicut.fsug.in" target="__blank" style="text-decoration: none">FSUG Calicut</a></b>');

var lbs = L.marker([12.50, 75.05], {
  icon: mapMarker
}).addTo(mymap).bindPopup('<b><a href="http://lbs.fsug.in" target="__blank" style="text-decoration: none">FSUG at LBS College of Engineering, Kasargode</a></b>');

var rset = L.marker([10.01, 76.34], {
  icon: mapMarker
}).addTo(mymap).bindPopup('<b><a href="https://riot.im/app/#/room/rsetfc:matrix.org" target="__blank" style="text-decoration: none">RSETFC (Rajagiri School of Engineering & Technology FOSS Club), Ernakulam</a></b>');

var fossclub = L.marker([10.00, 76.33], {
  icon: mapMarker
}).addTo(mymap).bindPopup('<b><a href="http://fossclub.in" target="__blank" style="text-decoration: none">FOSSClub, Ernakulam</a></b>');

var malappuram = L.marker([11.04, 76.08], {
  icon: mapMarker
}).addTo(mymap).bindPopup('<b><a href="https://www.freelists.org/list/ssug-malappuram" target="__blank" style="text-decoration: none">SSUG (Swathanthra Software Users Group) Malappuram</a></b>');

var mes = L.marker([10.84, 76.03], {
  icon: mapMarker
}).addTo(mymap).bindPopup('<b><a href="https://groups.google.com/forum/#!forum/mes-fsug" target="__blank" style="text-decoration: none">FSUG at MES College of Engineering, Malappuram</a></b>');

var thiruvananthapuram = L.marker([8.49, 76.95], {
  icon: mapMarker
}).addTo(mymap).bindPopup('<b><a href="https://www.freelists.org/list/ssug-malappuram" target="__blank" style="text-decoration: none">FSUG Thiruvananthapuram</a></b>');

var kollam = L.marker([8.88, 76.60], {
  icon: mapMarker
}).addTo(mymap).bindPopup('<b><a href="https://www.freelists.org/list/ssug-kollam" target="__blank" style="text-decoration: none">SSUG (Swathanthra Software Users Group) Kollam</a></b>');

var meerut = L.marker([28.99, 77.7], {
  icon: mapMarker
}).addTo(mymap).bindPopup('<b><a href="https://groups.google.com/forum/#!forum/glug-meerut" target="__blank" style="text-decoration: none">GLUG (Gnu/Linux Users Group) Meerut</a></b>');

var palakkad = L.marker([10.77, 76.65], {
  icon: mapMarker
}).addTo(mymap).bindPopup('<b><a href="http://plus.fosscommunity.in" target="__blank" style="text-decoration: none">PLUS (Palakkad Libre software Users Society)</a></b>');
